var WEBAPP_NAME = "sbits-zendesk";

var ZENDESK_FILE = "/_System/apps/" + WEBAPP_NAME + "/zendesk.inc";

var DATA_FILE = "/_System/apps/" + WEBAPP_NAME + "/_config/data.json";

var app = app || {};

$(function () {
    if($.cookie('mainUrl') == null){
        $.cookie('mainUrl', document.referrer, { path: '/' });
    }

    // Get the authorization token
    var access_token = BCAPI.Helper.Site.getAccessToken();

    app.Settings = Backbone.Model.extend({
        defaults: {
            "zSubdomain": ''
        }
    });
    app.settings = new app.Settings();

    app.MainSectionView = Backbone.View.extend({
        el: $('#container-body'),
        template: _.template($('#main-template').html()),
        filePath: '/_System/apps/'+WEBAPP_NAME+'/_config/data.json',
        events: {
            "click #save-button" : "save"
        },

        fid_count: function(value) {
            if(value == "") {
                return 0;
            }
            var fid_inputs = $('input.fid');
            var count = 0;
            for(var i=0; i<fid_inputs.length; i++) {
                if($(fid_inputs[i]).val() == value) {
                    count++;
                }
            }
            return count;
        },

        save: function() {
            app.settings.set("zShowWidget", $('input[name="zendesk-showwidget"]').prop('checked'));
            app.settings.set("zSubdomain", $('input[name="zendesk-subdomain"]').val());
            app.settings.set("zName", $('input[name="zendesk-name"]').prop('checked'));
            app.settings.set("zEmail", $('input[name="zendesk-email"]').prop('checked'));

            app.settings.set("zDeviceClass", $('input[name="zendesk-device-class"]').prop('checked'));
            app.settings.set("zDeviceClassFID", $('input[name="zendesk-device-class-fid"]').val());
            app.settings.set("zDeviceClassHF", $('input[name="zendesk-device-class-hf"]').prop('checked'));
            app.settings.set("zDeviceClassCMB", this.fid_count($('input[name="zendesk-device-class-fid"]').val())>1);

            app.settings.set("zIsLoggedIn", $('input[name="zendesk-isloggedin"]').prop('checked'));
            app.settings.set("zIsLoggedInFID", $('input[name="zendesk-isloggedin-fid"]').val());
            app.settings.set("zIsLoggedInHF", $('input[name="zendesk-isloggedin-hf"]').prop('checked'));
            app.settings.set("zIsLoggedInCMB", this.fid_count($('input[name="zendesk-isloggedin-fid"]').val())>1);

            app.settings.set("zEntityID", $('input[name="zendesk-entityid"]').prop('checked'));
            app.settings.set("zEntityIDFID", $('input[name="zendesk-entityid-fid"]').val());
            app.settings.set("zEntityIDHF", $('input[name="zendesk-entityid-hf"]').prop('checked'));
            app.settings.set("zEntityIDCMB", this.fid_count($('input[name="zendesk-entityid-fid"]').val())>1);

            app.settings.set("zCRMLink", $('input[name="zendesk-crmlink"]').prop('checked'));
            app.settings.set("zCRMLinkFID", $('input[name="zendesk-crmlink-fid"]').val());
            app.settings.set("zCRMLinkHF", $('input[name="zendesk-crmlink-hf"]').prop('checked'));
            app.settings.set("zCRMLinkCMB", this.fid_count($('input[name="zendesk-crmlink-fid"]').val())>1);

            app.settings.set("zCompany", $('input[name="zendesk-company"]').prop('checked'));
            app.settings.set("zCompanyFID", $('input[name="zendesk-company-fid"]').val());
            app.settings.set("zCompanyHF", $('input[name="zendesk-company-hf"]').prop('checked'));
            app.settings.set("zCompanyCMB", this.fid_count($('input[name="zendesk-company-fid"]').val())>1);

            app.settings.set("zWebsite", $('input[name="zendesk-website"]').prop('checked'));
            app.settings.set("zWebsiteFID", $('input[name="zendesk-website-fid"]').val());
            app.settings.set("zWebsiteHF", $('input[name="zendesk-website-hf"]').prop('checked'));
            app.settings.set("zWebsiteCMB", this.fid_count($('input[name="zendesk-website-fid"]').val())>1);

            app.settings.set("zPhone", $('input[name="zendesk-phone"]').prop('checked'));
            app.settings.set("zPhoneFID", $('input[name="zendesk-phone-fid"]').val());
            app.settings.set("zPhoneHF", $('input[name="zendesk-phone-hf"]').prop('checked'));
            app.settings.set("zPhoneCMB", this.fid_count($('input[name="zendesk-phone-fid"]').val())>1);

            app.settings.set("zDOB", $('input[name="zendesk-dob"]').prop('checked'));
            app.settings.set("zDOBFID", $('input[name="zendesk-dob-fid"]').val());
            app.settings.set("zDOBHF", $('input[name="zendesk-dob-hf"]').prop('checked'));
            app.settings.set("zDOBCMB", this.fid_count($('input[name="zendesk-dob-fid"]').val())>1);

            app.settings.set("zCustomerType", $('input[name="zendesk-customertype"]').prop('checked'));
            app.settings.set("zCustomerTypeFID", $('input[name="zendesk-customertype-fid"]').val());
            app.settings.set("zCustomerTypeHF", $('input[name="zendesk-customertype-hf"]').prop('checked'));
            app.settings.set("zCustomerTypeCMB", this.fid_count($('input[name="zendesk-customertype-fid"]').val())>1);

            app.settings.set("zIndustry", $('input[name="zendesk-industry"]').prop('checked'));
            app.settings.set("zIndustryFID", $('input[name="zendesk-industry-fid"]').val());
            app.settings.set("zIndustryHF", $('input[name="zendesk-industry-hf"]').prop('checked'));
            app.settings.set("zIndustryCMB", this.fid_count($('input[name="zendesk-industry-fid"]').val())>1);

            app.settings.set("zRating", $('input[name="zendesk-rating"]').prop('checked'));
            app.settings.set("zRatingFID", $('input[name="zendesk-rating-fid"]').val());
            app.settings.set("zRatingHF", $('input[name="zendesk-rating-hf"]').prop('checked'));
            app.settings.set("zRatingCMB", this.fid_count($('input[name="zendesk-rating-fid"]').val())>1);

            app.settings.set("zIsWholesaler", $('input[name="zendesk-iswholesaler"]').prop('checked'));
            app.settings.set("zIsWholesalerFID", $('input[name="zendesk-iswholesaler-fid"]').val());
            app.settings.set("zIsWholesalerHF", $('input[name="zendesk-iswholesaler-hf"]').prop('checked'));
            app.settings.set("zIsWholesalerCMB", this.fid_count($('input[name="zendesk-iswholesaler-fid"]').val())>1);

            var file = BCAPI.Models.FileSystem.Root.file(this.filePath);
            file.upload(JSON.stringify(app.settings.toJSON()));
            systemNotifications.showSuccess('Saved', 'Widget code is ready. See the instructions how to include code to your site.');
        },
        initialize: function() {
            var file = BCAPI.Models.FileSystem.Root.file(this.filePath);
            file.download().done(function(content) {
                app.settings.set(JSON.parse(content));
                app.mainSectionView.render();
            });
        },
        render: function() {
            $('#hybridSubmenu li').removeClass('active');
            $('#tab-main').addClass('active');
            var context = {
                'zendesk_showwidget': app.settings.get('zShowWidget')?'checked':'',
                'zendesk_subdomain': app.settings.get('zSubdomain',''),
                'zendesk_name': app.settings.get('zName')?'checked':'',
                'zendesk_email': app.settings.get('zEmail')?'checked':'',

                'zendesk_device_class': app.settings.get('zDeviceClass')?'checked':'',
                'zendesk_device_class_fid': app.settings.get('zDeviceClassFID',''),
                'zendesk_device_class_hf': app.settings.get('zDeviceClassHF')?'checked':'',

                'zendesk_isloggedin': app.settings.get('zIsLoggedIn')?'checked':'',
                'zendesk_isloggedin_fid': app.settings.get('zIsLoggedInFID',''),
                'zendesk_isloggedin_hf': app.settings.get('zIsLoggedInHF')?'checked':'',

                'zendesk_entityid': app.settings.get('zEntityID')?'checked':'',
                'zendesk_entityid_fid': app.settings.get('zEntityIDFID',''),
                'zendesk_entityid_hf': app.settings.get('zEntityIDHF')?'checked':'',

                'zendesk_crmlink': app.settings.get('zCRMLink')?'checked':'',
                'zendesk_crmlink_fid': app.settings.get('zCRMLinkFID',''),
                'zendesk_crmlink_hf': app.settings.get('zCRMLinkHF')?'checked':'',

                'zendesk_company': app.settings.get('zCompany')?'checked':'',
                'zendesk_company_fid': app.settings.get('zCompanyFID',''),
                'zendesk_company_hf': app.settings.get('zCompanyHF')?'checked':'',

                'zendesk_website': app.settings.get('zWebsite')?'checked':'',
                'zendesk_website_fid': app.settings.get('zWebsiteFID',''),
                'zendesk_website_hf': app.settings.get('zWebsiteHF')?'checked':'',

                'zendesk_phone': app.settings.get('zPhone')?'checked':'',
                'zendesk_phone_fid': app.settings.get('zPhoneFID',''),
                'zendesk_phone_hf': app.settings.get('zPhoneHF')?'checked':'',

                'zendesk_dob': app.settings.get('zDOB')?'checked':'',
                'zendesk_dob_fid': app.settings.get('zDOBFID',''),
                'zendesk_dob_hf': app.settings.get('zDOBHF')?'checked':'',

                'zendesk_customertype': app.settings.get('zCustomerType')?'checked':'',
                'zendesk_customertype_fid': app.settings.get('zCustomerTypeFID',''),
                'zendesk_customertype_hf': app.settings.get('zCustomerTypeHF')?'checked':'',

                'zendesk_industry': app.settings.get('zIndustry')?'checked':'',
                'zendesk_industry_fid': app.settings.get('zIndustryFID',''),
                'zendesk_industry_hf': app.settings.get('zIndustryHF')?'checked':'',

                'zendesk_rating': app.settings.get('zRating')?'checked':'',
                'zendesk_rating_fid': app.settings.get('zRatingFID',''),
                'zendesk_rating_hf': app.settings.get('zRatingHF')?'checked':'',

                'zendesk_iswholesaler': app.settings.get('zIsWholesaler')?'checked':'',
                'zendesk_iswholesaler_fid': app.settings.get('zIsWholesalerFID',''),
                'zendesk_iswholesaler_hf': app.settings.get('zIsWholesalerHF')?'checked':''
            }
            this.$el.html(this.template(context));
        }
    });
    app.mainSectionView = new app.MainSectionView();

    app.HelpView = Backbone.View.extend({
        el: $('#container-body'),
        template: _.template($('#help-template').html()),
        events: {
            "click #uninstall-button": "uninstall"
        },
        render: function() {
            $('#hybridSubmenu li').removeClass('active');
            $('#tab-help').addClass('active');               
            this.$el.html(this.template());
        },
        uninstall:function(){            
            app.uninstallApp();
        }
    });
    app.helpView = new app.HelpView();

    app.redirectToDashboard = function() {
        var parentLocation = $.cookie('mainUrl');
        var dashboardUrl = parentLocation.substring(0, parentLocation.indexOf("/Admin")) + "/Admin/Dashboard_Business.aspx";
        $.removeCookie('mainUrl', { path: '/' });
        window.parent.location = dashboardUrl;
    }

    app.uninstallApp = function() {
        if(confirm("Uninstall operation cannot be undone. Are you sure?")) {
            // Uninstall the application
            var appPath = "/_System/apps/" + WEBAPP_NAME;
            var redirected = false;
            var appFolder = new BCAPI.Models.FileSystem.Folder(appPath);

            appFolder.destroy().always(function() {
                if(!redirected) {
                    redirected = true;
                    app.redirectToDashboard();         
                }       
            });
        }
    }

    app.Router = Backbone.Router.extend({
        routes: {
            "main-page": "mainSection",
            "help": "helpSection"
        },
        helpSection: function() {
            app.helpView.render();
        },
        mainSection: function() {
            app.mainSectionView.render();
        }
    });
    app.router = new app.Router();
    Backbone.history.start();
    app.router.navigate("main-page", {trigger:true});
});
