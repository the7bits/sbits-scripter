{% if this.zShowWidget -%}
<script>/*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(c){n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',o=s}o.open()._l=function(){var o=this.createElement("script");n&&(this.domain=n),o.id="js-iframe-async",o.src=e,this.t=+new Date,this.zendeskHost=t,this.zEQueue=a,this.body.appendChild(o)},o.write('<body onload="document._l();">'),o.close()}("//assets.zendesk.com/embeddable_framework/main.js","{{this.zSubdomain}}.zendesk.com");/*]]>*/</script>

<script type="text/javascript">
{% if this.globals.user.isLoggedIn -%}
{% if this.zName or this.zEmail -%}
zE(function(){
    var settings = {};
    {% if this.zName -%}settings.name = "{{this.globals.user.fullname}}";{% endif %}
    {% if this.zEmail -%}settings.email = "{{this.globals.user.email}}";{% endif %}

    zE.identify(settings);
});
{% endif -%}
{% endif -%}

{% if this.zDeviceClass or this.zIsLoggedIn or this.zEntityID or this.zCRMLink or this.Company or this.zWebsite or this.zPhone or this.zDOB or this.zCustomerType or this.zIndustry or this.zRating or this.isWholesaler  %}
window.ZENDESK_WIDGET_CHECK_TIMEOUT = 1000;
window.ZENDESK_WIDGET_FILL_TIMEOUT = 1000;
function fillZendeskWidgetData() {
    var old_val = "";
    {% if this.zDeviceClass %}
    old_val = $('#ticketSubmissionForm').contents().find('[name="{{this.zDeviceClassFID}}"]').val();
    $('#ticketSubmissionForm').contents().find('[name="{{this.zDeviceClassFID}}"]').val({% if this.zDeviceClassCMB %}old_val+'Device class: '+{% endif %}'{{this.globals.visitor.deviceClass}}'{% if this.zDeviceClassCMB %}+'; \n'{% endif %});
        {% if this.zDeviceClassHF %}
        $('#ticketSubmissionForm').contents().find('[name="{{this.zDeviceClassFID}}"]').parent().hide();
        {% endif -%}
    {% endif -%}
    {% if this.zIsLoggedIn %}
    old_val = $('#ticketSubmissionForm').contents().find('[name="{{this.zIsLoggedInFID}}"]').val();
    $('#ticketSubmissionForm').contents().find('[name="{{this.zIsLoggedInFID}}"]').val({% if this.zIsLoggedInCMB %}old_val+'Is logged in: '+{% endif %}'{{this.globals.user.isLoggedIn}}'{% if this.zIsLoggedInCMB %}+'; \n'{% endif %});
        {% if this.zIsLoggedInHF %}
        $('#ticketSubmissionForm').contents().find('[name="{{this.zIsLoggedInFID}}"]').parent().hide();
        {% endif -%}
    {% endif -%}
    {% if this.zEntityID %}
    old_val = $('#ticketSubmissionForm').contents().find('[name="{{this.zEntityIDFID}}"]').val();
    $('#ticketSubmissionForm').contents().find('[name="{{this.zEntityIDFID}}"]').val({% if this.zEntityIDCMB %}old_val+'CRM ID: '+{% endif %}'{{this.globals.user.entityId}}'{% if this.zEntityIDCMB %}+'; \n'{% endif %});
        {% if this.zEntityIDHF %}
        $('#ticketSubmissionForm').contents().find('[name="{{this.zEntityIDFID}}"]').parent().hide();
        {% endif -%}
    {% endif -%}

    {% if this.zCRMLink %}
    old_val = $('#ticketSubmissionForm').contents().find('[name="{{this.zCRMLinkFID}}"]').val();
    $('#ticketSubmissionForm').contents().find('[name="{{this.zCRMLinkFID}}"]').val({% if this.zCRMLinkCMB %}old_val+'CRM link: '+{% endif %}'{{this.globals.site.host}}/CRM/MemberDetailsv2.aspx?EntityID={{this.globals.user.entityId}}'{% if this.zCRMLinkCMB %}+'; \n'{% endif %});
        {% if this.zCRMLinkHF %}
        $('#ticketSubmissionForm').contents().find('[name="{{this.zCRMLinkFID}}"]').parent().hide();
        {% endif -%}
    {% endif -%}

    {% if this.zCompany %}
    old_val = $('#ticketSubmissionForm').contents().find('[name="{{this.zCompanyFID}}"]').val();
    $('#ticketSubmissionForm').contents().find('[name="{{this.zCompanyFID}}"]').val({% if this.zCompanyCMB %}old_val+'Company: '+{% endif %}'{module_company}'{% if this.zCompanyCMB %}+'; \n'{% endif %});
        {% if this.zCompanyHF %}
        $('#ticketSubmissionForm').contents().find('[name="{{this.zCompanyFID}}"]').parent().hide();
        {% endif -%}
    {% endif -%}
    {% if this.zWebsite %}
    old_val = $('#ticketSubmissionForm').contents().find('[name="{{this.zWebsiteFID}}"]').val();
    $('#ticketSubmissionForm').contents().find('[name="{{this.zWebsiteFID}}"]').val({% if this.zWebsiteCMB %}old_val+'Website :'+{% endif %}'{module_webaddress}'{% if this.zWebsiteCMB %}+'; \n'{% endif %});
        {% if this.zWebsiteHF %}
        $('#ticketSubmissionForm').contents().find('[name="{{this.zWebsiteFID}}"]').parent().hide();
        {% endif -%}
    {% endif -%}
    {% if this.zPhone %}
    old_val = $('#ticketSubmissionForm').contents().find('[name="{{this.zPhoneFID}}"]').val();
    $('#ticketSubmissionForm').contents().find('[name="{{this.zPhoneFID}}"]').val({% if this.zPhoneCMB %}old_val+'Phone: '+{% endif %}'{module_cellphone}'{% if this.zPhoneCMB %}+'; \n'{% endif %});
        {% if this.zPhoneHF %}
        $('#ticketSubmissionForm').contents().find('[name="{{this.zPhoneFID}}"]').parent().hide();
        {% endif -%}
    {% endif -%}
    {% if this.zDOB %}
    old_val = $('#ticketSubmissionForm').contents().find('[name="{{this.zDOBFID}}"]').val();
    $('#ticketSubmissionForm').contents().find('[name="{{this.zDOBFID}}"]').val({% if this.zDOBCMB %}old_val+'Date of birth: '+{% endif %}'{module_dob}'{% if this.zDOBCMB %}+'; \n'{% endif %});
        {% if this.zDOBHF %}
        $('#ticketSubmissionForm').contents().find('[name="{{this.zDOBFID}}"]').parent().hide();
        {% endif -%}
    {% endif -%}

    {% if this.zCustomerType %}
    old_val = $('#ticketSubmissionForm').contents().find('[name="{{this.zCustomerTypeFID}}"]').val();
    $('#ticketSubmissionForm').contents().find('[name="{{this.zCustomerTypeFID}}"]').val({% if this.zCustomerTypeCMB %}old_val+'Customer type: '+{% endif %}'{{this.globals.user.customerType.name}} - {{this.globals.user.customerType.id}}'{% if this.zCustomerTypeCMB %}+'; \n'{% endif %});
        {% if this.zCustomerTypeHF %}
        $('#ticketSubmissionForm').contents().find('[name="{{this.zCustomerTypeFID}}"]').parent().hide();
        {% endif -%}
    {% endif -%}
    {% if this.zIndustry %}
    old_val = $('#ticketSubmissionForm').contents().find('[name="{{this.zIndustryFID}}"]').val();
    $('#ticketSubmissionForm').contents().find('[name="{{this.zIndustryFID}}"]').val({% if this.zIndustryCMB %}old_val+'Industry: '+{% endif %}'{{this.globals.user.industryType.name}} - {{this.globals.user.industryType.id}}'{% if this.zIndustryCMB %}+'; \n'{% endif %});
        {% if this.zIndustryHF %}
        $('#ticketSubmissionForm').contents().find('[name="{{this.zIndustryFID}}"]').parent().hide();
        {% endif -%}
    {% endif -%}
    {% if this.zRating %}
    old_val = $('#ticketSubmissionForm').contents().find('[name="{{this.zRatingFID}}"]').val();
    $('#ticketSubmissionForm').contents().find('[name="{{this.zRatingFID}}"]').val({% if this.zRatingCMB %}old_val+'Rating: '+{% endif %}'{{this.globals.user.ratingType.name}} - {{this.globals.user.ratingType.id}}'{% if this.zRatingCMB %}+'; \n'{% endif %});
        {% if this.zRatingHF %}
        $('#ticketSubmissionForm').contents().find('[name="{{this.zRatingFID}}"]').parent().hide();
        {% endif -%}
    {% endif -%}

    {% if this.zIsWholesaler %}
    old_val = $('#ticketSubmissionForm').contents().find('[name="{{this.zIsWholesalerFID}}"]').val();
    $('#ticketSubmissionForm').contents().find('[name="{{this.zIsWholesalerFID}}"]').val({% if this.zIsWholesalerCMB %}old_val+'Is wholesaler: '+{% endif %}'{{this.globals.user.isWholesaler}}'{% if this.zIsWholesalerCMB %}+'; \n'{% endif %});
        {% if this.zIsWholesalerHF %}
        $('#ticketSubmissionForm').contents().find('[name="{{this.zIsWholesalerFID}}"]').parent().hide();
        {% endif -%}
    {% endif -%}

    {% if this.zDeviceClassHF or this.zIsLoggedInHF or this.zEntityIDHF or this.zIsWholesalerHF or this.zCustomerType or this.zIndustry or this.zRating or this.zCompany or this.zWebsite or this.zPhone or this.zDOB or this.zCRMLink %}
    zE(function(){
        zE.hide(); zE.show();
    });
    {% endif -%}
}

function checkZendeskWidgetData() {
    var _frame = $('#ticketSubmissionForm');
    if(_frame.length > 0) {
        setTimeout(fillZendeskWidgetData, window.ZENDESK_WIDGET_FILL_TIMEOUT);
    } else {
        setTimeout(checkZendeskWidgetData, window.ZENDESK_WIDGET_CHECK_TIMEOUT);
    }
}

$(document).ready(function(){
    setTimeout(checkZendeskWidgetData, window.ZENDESK_WIDGET_CHECK_TIMEOUT);
});
{% endif -%}
{% endif -%}
</script>